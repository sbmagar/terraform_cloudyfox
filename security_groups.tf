resource "aws_security_group" "tf_ec2_security_pub" {
  name        = "tf_ec2_security_pub"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_default_vpc.Main.id

  ingress = [
      {
      description      = "Inbound rules from VPC"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = [aws_default_vpc.Main.cidr_block]
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      security_groups = []
      self = true
    }
  ]
  

  egress = [      
    {
      description      = "Inbound rules from VPC"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
    #   protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      security_groups = []
      self = true
    }
  ]

  tags = {
    Name = "tf_ec2_security_pub"
    instance_name = "Terraform_Instance"
  }
}


resource "aws_security_group" "tf_ec2_security_priv" {
  name        = "tf_ec2_security_priv"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_default_vpc.Main.id

  ingress = [
      {
      description      = "Inbound rules from VPC"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
    #   cidr_blocks = ["10.0.0.0/16"]
      cidr_blocks      = [aws_default_vpc.Main.cidr_block]
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      security_groups = []
      self = true
    }
  ]

  egress = [
      {
      description      = "Inbound rules from VPC"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
    #   protocol         = "tcp"
      cidr_blocks      = "${var.private_subnets}"
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      security_groups = []
      self = true
    }
  ]

  tags = {
    Name = "tf_ec2_security_priv"
    instance_name = "Terraform_Instance"
  }
}
