region = "ap-south-1"
main_vpc_cidr = "10.0.0.0/16"
public_subnets = "10.0.0.128/18"
private_subnets = "10.0.0.192/18"