# Internet Gateway
resource "aws_internet_gateway" "myIGW" {    
    vpc_id =  aws_default_vpc.Main.id
}

# Defining public subnet
resource "aws_subnet" "public" {
    vpc_id = aws_default_vpc.Main.id

    # cidr_block = "192.168.1.0/16"
    cidr_block = "${var.public_subnets}"
    availability_zone = "ap-south-1a"
  
}

#Defining private subnet
resource "aws_subnet" "private" {
    vpc_id = aws_default_vpc.Main.id

    # cidr_block = "192.168.64.0/16"
    cidr_block = "${var.private_subnets}"
    availability_zone = "ap.south.1a"
  
}


# route table for public subnet
resource "aws_route_table" "PublicRT" {    
    vpc_id =  aws_default_vpc.Main.id
    route {
        cidr_block = "0.0.0.0/0" # public -> internet
        gateway_id = aws_internet_gateway.myIGW.id
    }
}

# private route
resource "aws_route_table" "PrivateRT" {
    vpc_id = aws_default_vpc.Main.id
    route {
        cidr_block = "0.0.0.0/0" # private -> internet
        nat_gateway_id = aws_nat_gateway.NATgw.id
    }
}

resource "aws_route_table_association" "PublicRTassociation" {
    subnet_id = aws_subnet.public.id
    route_table_id = aws_route_table.PublicRT.id
}

resource "aws_route_table_association" "PrivateRTassociation" {
    subnet_id = aws_subnet.private.id
    route_table_id = aws_route_table.PrivateRT.id
}
resource "aws_eip" "nateIP" {
    vpc   = true
}
resource "aws_nat_gateway" "NATgw" {
    allocation_id = aws_eip.nateIP.id
    subnet_id = aws_subnet.public.id
}