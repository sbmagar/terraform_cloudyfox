# Managing multiple environments on Terraform

Dev/stage/uat/prod

## 1. Single tf file method
- One way is with single tf file. create develop.tf file and write VPCs, EC2s or whatever in it. Soon, create another environment. Let's call it staging.tf which is going to be still a single tfstate file. And so on...

## 2. Workspaces method 
- Terraform workspaces is also a another way of managing multiple envs. The goal of workspaces is to separate one tfstate file per environment. Should specify separate variables for every environment and separate state for each environment.

## 3. Individual directory method
- Also using different folders in a git repo for different environments is another way of managing multiple envs. Like naming folders as "dev", "stage", "prod", etc

## 4. Module method
- Moduling is another best way of managing environments. In this, create multiple folders, one for each environments and first Terraform file will just specify what address space staging vpc will have, while the other will have another address space. So solve that issue just by injecting different values in modules.


