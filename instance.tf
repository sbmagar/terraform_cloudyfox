# Create public insatance with public security group
resource "aws_instance" "ec2_public" {
  ami                         = "ami-656dfwe561"
  availability_zone           = "ap-south-1a"
  associate_public_ip_address = true
  instance_type               = "t2.micro"
  key_name                    = "${var.key_name}"
  subnet_id                   = "${aws_subnet.public.id}"
  vpc_security_group_ids      = ["${aws_security_group.tf_ec2_security_pub.id}"]

  tags = {
    "Name" = "ec2_public"
  }
}

resource "local_file" "environment" {
  content = var.environment
  filename = "/tmp/filename"
  
}